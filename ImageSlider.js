import { useEffect, useState } from "react";

const slideStyles = {
    width: "100%",
    height: "100%",
    backgroundSize: "cover",
    backgroundPosition: "center",
};

const rightArrowStyles = {
    position: "absolute",
    top: "50%",
    transform: "translate(0, -50%)",
    right: "32px",
    fontSize: "30px",
    color: "#fff",
    cursor: "pointer",
};

const leftArrowStyles = {
    position: "absolute",
    top: "50%",
    transform: "translate(0, -50%)",
    left: "32px",
    fontSize: "30px",
    color: "#fff",
    cursor: "pointer",
};

const sliderStyles = {
    position: "relative",
    height: "150%",
};





const ImageSlider = ({ slides }) => {
    const [currentIndex, setCurrentIndex] = useState(0);

    const Prev = () => {
        const isFirstSlide = currentIndex === 0;
        const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
        setCurrentIndex(newIndex);
    }
    const Next = () => {
        const isLastSlide = currentIndex === slides.length - 1;
        const newIndex = isLastSlide ? 0 : currentIndex + 1;
        setCurrentIndex(newIndex);
    };

    const slideStylesWidthBackground = {
        ...slideStyles,
        backgroundImage: `url(${slides[currentIndex].url})`,
    };
    useEffect(() => {

        const interval = setInterval(() => {
            setCurrentIndex((current) =>
                current === slides.length - 1 ? 0 : current + 1
            )
        }, 7000)

        return () => clearInterval()
    }, [])


    return (
        <div style={sliderStyles}>
            <div>
                <div onClick={Prev} style={leftArrowStyles}>
                    ❰
                </div>
                <div onClick={Next} style={rightArrowStyles}>
                    ❱
                </div>
            </div>
            <div style={slideStylesWidthBackground}></div>

        </div>
    );

}

export default ImageSlider;