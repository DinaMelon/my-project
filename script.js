function getRandom(min, max) // функция для рандома
{
    return Math.floor(Math.random() * (max + 1 - min));
}

var n = prompt('Введите длину массива'); // количество элементов в массиве
var arr = [];
for (var i = 0; i < n; i++) {
    arr[i] = getRandom(0, 100);
    document.write(arr[i] + '  '); // вывод сгенерированного массива 
}
document.write(';', ' ');
// поиск четных и суммы их квадратов
var i;
var sum = 0;
var result = [];
for (i = 0; i < n; i++) {
    if (arr[i] % 2 == 0)
        result.push(arr[i]);
}
for (i = 0; i < result.length; i++) {
    sum += (result[i] ** 2);
}
console.log(sum);
