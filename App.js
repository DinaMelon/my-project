import ImageSlider from "./ImageSlider";
const App = () => {
  const slides = [
    { url: "https://vsegda-pomnim.com/uploads/posts/2022-04/1651203901_1-vsegda-pomnim-com-p-ochen-mnogo-fruktov-foto-1.jpg" },
    { url: "https://vsegda-pomnim.com/uploads/posts/2022-04/1651229568_18-vsegda-pomnim-com-p-letnie-frukti-foto-18.jpg" },
    { url: "https://demotivation.ru/wp-content/uploads/2020/08/frukty-banan-arbuz-ananas-2048x1280.jpg" },
  ];
  const containerStyles = {
    width: "1000px",
    height: "280px",
    margin: "0 auto",
  };
  return (
    <div>

      <div style={containerStyles}>
        <ImageSlider slides={slides} />
      </div>
    </div>
  );
};

export default App;